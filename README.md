# miro-random-emoji
_Add a random set of emojis to your miro.com board. Written as a plugin for miro.com_

For further information see https://theomega.gitlab.io/miro-random-emoji
