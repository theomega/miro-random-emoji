const fs = require('fs').promises
const fetch = require('node-fetch')

const categories = ['people-body', 'animals-nature', 'food-drink', 'travel-places', 'activities', 'objects']
const blacklistSubCategories = ['hand-fingers-open', 'hand-fingers-partial', 'hand-single-finger', 'hand-fingers-closed', 'hands', 'time', 'family', 'person-symbol', 'person-gesture', 'people-body', 'person']

// Uses the emoji api to fetch all emojis in a provided category, returns them as an array
async function getEmojisFromCategory (c) {
  const e = await fetch('https://emoji-api.com/categories/' + c + '?access_key=08a8350369a9c1e52e0b50f41c270ba988acb019')
  const eb = await e.json()
  const ebf = eb.filter((e) => !blacklistSubCategories.includes(e.subGroup)).filter(e => !e.slug.includes('skin-tone') && !e.slug.startsWith('man-') && !e.slug.startsWith('woman-') && !e.slug.startsWith('women-') && !e.slug.startsWith('men-') && !e.slug.match(/^e[0-9]/g)).map(e => {
    delete e.variants
    delete e.unicodeName
    delete e.codePoint
    delete e.group
    delete e.subGroup
    delete e.parent
    return e
  })
  console.log('Filtered emojis for category ' + c, ebf)

  return ebf
}

// Fetches all emojis we want to use by combining the list for different hard coded categories
async function fetchAllEmojis () {
  return (await Promise.all(categories.map(getEmojisFromCategory))).flat()
}

async function main () {
  const emojis = await fetchAllEmojis()
  await fs.writeFile('public/emojis.json', JSON.stringify(emojis, null, 1))
}

main().then(() => {})
