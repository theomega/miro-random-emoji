/* global miro */
// Fetches all emojis we want to use by combining the list for different hard coded categories
async function fetchAllEmojis () {
  const request = await window.fetch('./emojis.json')
  return (await request.json())
}

// Returns one random emoji
let allEmojis
async function getRandomEmoji () {
  // Check if there is already a list of all emojis to avoid another fetch
  if (!allEmojis) {
    allEmojis = await fetchAllEmojis()
    console.log('fetched ' + allEmojis.length + ' emojis')
  }

  // Randomly pick one emoji from the big list
  const emoji = allEmojis[Math.floor(Math.random() * allEmojis.length)]

  return emoji
}

// Returns `count` random emojis, preventing duplicates
async function getRandomEmojis (count) {
  const re = []
  while (re.length < count) {
    const candidate = await getRandomEmoji()

    // Check if we already choose the emoji
    if (!re.find((e) => (e.slug === candidate.slug))) {
      console.log('Picked', candidate)
      re.push(candidate)
    } else {
      console.log('Skipped', candidate)
    }
  }

  console.log('Found emojis', re)
  return re
}

miro.onReady(() => {
  miro.initialize({
    extensionPoints: {
      bottomBar: async () => {
        const authorized = await miro.isAuthorized()
        if (authorized) {
          return {
            title: 'Five Random Emojis',
            svgIcon: '<path fill="currentColor" d="m 18.23808,13.55952 a 0.77976,0.77976 0 0 0 -0.77976,0.77976 0.77976,0.77976 0 0 0 0.77976,0.77976 0.77976,0.77976 0 0 0 0.77976,-0.77976 0.77976,0.77976 0 0 0 -0.77976,-0.77976 z m -4.67856,0 a 0.77976,0.77976 0 0 0 -0.77976,0.77976 0.77976,0.77976 0 0 0 0.77976,0.77976 0.77976,0.77976 0 0 0 0.77976,-0.77976 0.77976,0.77976 0 0 0 -0.77976,-0.77976 z m 0,-4.6785601 a 0.77976,0.77976 0 0 0 -0.77976,0.77976 0.77976,0.77976 0 0 0 0.77976,0.7797601 0.77976,0.77976 0 0 0 0.77976,-0.7797601 0.77976,0.77976 0 0 0 -0.77976,-0.77976 z m 4.67856,0 a 0.77976,0.77976 0 0 0 -0.77976,0.77976 0.77976,0.77976 0 0 0 0.77976,0.7797601 0.77976,0.77976 0 0 0 0.77976,-0.7797601 0.77976,0.77976 0 0 0 -0.77976,-0.77976 z M 15.8988,11.22024 A 0.77976,0.77976 0 0 0 15.11904,12 0.77976,0.77976 0 0 0 15.8988,12.77976 0.77976,0.77976 0 0 0 16.67856,12 0.77976,0.77976 0 0 0 15.8988,11.22024 Z M 12.77976,6.5416799 c -1.283061,0 -2.33928,1.0562218 -2.33928,2.33928 V 15.11904 c 0,1.283058 1.056219,2.33928 2.33928,2.33928 h 6.23808 c 1.283058,0 2.33928,-1.056222 2.33928,-2.33928 V 8.8809599 c 0,-1.2830582 -1.056222,-2.33928 -2.33928,-2.33928 z m 0,1.55952 h 6.23808 c 0.432414,0 0.77976,0.3473495 0.77976,0.77976 V 15.11904 c 0,0.432414 -0.347346,0.77976 -0.77976,0.77976 H 12.77976 C 12.347346,15.8988 12,15.551454 12,15.11904 V 8.8809599 c 0,-0.4324105 0.347346,-0.77976 0.77976,-0.77976 z m -7.7975999,-7.01784 c -1.2830587,0 -2.33928,1.0562205 -2.33928,2.33928 V 20.57736 c 0,1.283058 1.0562213,2.33928 2.33928,2.33928 H 12.77976 c 1.283058,0 2.33928,-1.056222 2.33928,-2.33928 v -3.8988 a 0.77976,0.77976 0 0 0 -0.77976,-0.77976 0.77976,0.77976 0 0 0 -0.77976,0.77976 v 3.8988 c 0,0.432414 -0.347346,0.77976 -0.77976,0.77976 H 4.9821601 c -0.4324138,0 -0.77976,-0.347346 -0.77976,-0.77976 V 3.4226399 c 0,-0.4324127 0.3473462,-0.77976 0.77976,-0.77976 H 12.77976 c 0.432414,0 0.77976,0.3473473 0.77976,0.77976 v 3.8988 a 0.77976,0.77976 0 0 0 0.77976,0.77976 0.77976,0.77976 0 0 0 0.77976,-0.77976 v -3.8988 c 0,-1.2830595 -1.056222,-2.33928 -2.33928,-2.33928 z m 1.55952,0 a 0.77976,0.77976 0 0 0 -0.77976,0.77976 v 1.0142977 c 0,1.1638635 0.940879,2.1047423 2.1047433,2.1047423 H 9.8952577 C 11.059121,4.9821599 12,4.0412812 12,2.8774176 V 1.8631199 a 0.77976,0.77976 0 0 0 -0.77976,-0.77976 0.77976,0.77976 0 0 0 -0.77976,0.77976 v 1.0142977 c 0,0.2397031 -0.305517,0.5452223 -0.5452223,0.5452223 H 7.8666634 c -0.2397046,0 -0.5452233,-0.3055192 -0.5452233,-0.5452223 V 1.8631199 a 0.77976,0.77976 0 0 0 -0.77976,-0.77976 z m 0,17.1547201 a 0.77976,0.77976 0 0 0 -0.77976,0.77976 0.77976,0.77976 0 0 0 0.77976,0.77976 H 11.22024 A 0.77976,0.77976 0 0 0 12,19.01784 0.77976,0.77976 0 0 0 11.22024,18.23808 Z" /> ',
            onClick: async () => {
              const emojis = await getRandomEmojis(5)
              const sp = (await miro.board.widgets.create({ type: 'text', width: '150', scale: 3, text: emojis.map((e) => e.character).join(' ') }))[0]
              await miro.board.viewport.zoomToObject(sp)
            }
          }
        }
      }
    }
  })
})
